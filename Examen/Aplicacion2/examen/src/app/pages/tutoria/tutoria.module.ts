import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TutoriaPageRoutingModule } from './tutoria-routing.module';

import { TutoriaPage } from './tutoria.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TutoriaPageRoutingModule
  ],
  declarations: [TutoriaPage]
})
export class TutoriaPageModule {}

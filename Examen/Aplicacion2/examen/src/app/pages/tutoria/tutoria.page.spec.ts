import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TutoriaPage } from './tutoria.page';

describe('TutoriaPage', () => {
  let component: TutoriaPage;
  let fixture: ComponentFixture<TutoriaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TutoriaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TutoriaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { from } from 'rxjs';
import { ICrud } from 'src/app/interfaces/ICrud';
import { alumno, detalle,Tutoria } from 'src/app/interfaces/tutoria';
import { TutoriaService } from 'src/app/services/tutoria.service';

@Component({
  selector: 'app-tutoria',               
  templateUrl: './tutoria.page.html',
  styleUrls: ['./tutoria.page.scss'],
})
export class TutoriaPage implements OnInit, ICrud {              
       
 

  //variable tutoriaAuxiliar
 public Nivel="";
 public Observacion="";
  public tutoriaAuxiliar:Tutoria={codigo:'000', docente:'', date:'', identDocente:'',silabo:[], Cantidadtemas:'', alumnos:[], asignatura:'', tema:''};
  //Arreglo de tutorías

  
  public tutoriasAuxiliar:{}[]=
  [
    
  ];    

  public Detalle: {id:string, nivel:string}[]=
  [
    {id:'Alto', nivel:'Alto'},
    {id:'Medio', nivel:'Medio'},
    {id:'Bajo', nivel:'Bajo'},
    {id:'Ninguno', nivel:'Ninguno'}
  ];
  
  public alumnoAuxiliar: alumno={identificacion:'',nombre:''};
   
  public detalleAuxiliar: detalle={asignatura:'', temas:''};

  public Silabo:{silabo:string}[]= 
  [
    {silabo:'Unidad 1'},
    {silabo:'Unidad 2'},
    {silabo:'Unidad 3'},
    {silabo:'Unidad 4'},
    {silabo:'Unidad 5'},
    {silabo:'Unidad 6'},
  ]
  public date: String = new Date().toISOString();
  constructor( private cliente: TutoriaService, private toast:ToastController) {
    
   }

  async mostrarMensaje(mensaje:string, duracion:number){
    const mensajex= await this.toast.create({message:mensaje, duration:duracion})
    mensajex.present();
  }

  consultar():void{
    this.cliente.getTutorias().then(respuesta=>{
      this.tutoriasAuxiliar=[];
      for(let elemento in respuesta){
        this.tutoriasAuxiliar.push(respuesta[elemento]);
      }
    }).catch(err=>{
      console.log(err)
    })
  }

  consulIdent(identDocente:string){
    console.log("identDocente="+identDocente)
    this.cliente.getTutorias(identDocente).then(respuesta=>{
      this.tutoriasAuxiliar=[];
    for(let elemento in respuesta){
      this.tutoriasAuxiliar.push(respuesta[elemento]);
    }
    })
  }


  eliminar(): void {
   this.cliente.deleteTutorias(this.tutoriaAuxiliar.codigo).then(respuesta=>{
     this.mostrarMensaje('Se eliminó correctamente',2000)
   }).catch(error=>{
     this.mostrarMensaje('No se pudo eliminar la Tutoría',2000)
   })
  }       

  ngOnInit() {
  }

  nueva(){
    var contador=+1;
    var codigoTutoria="";
    this.cliente.getTutorias().then(respuesta=>{
      for(let elemento in respuesta){
        contador= contador+1;
      }
      codigoTutoria="00" + contador.toString();
      this.tutoriaAuxiliar={codigo:codigoTutoria, docente:'', date:'',silabo:[],identDocente:'', Cantidadtemas:'', alumnos:[] , asignatura:'', tema:'' }
    })
      //this.tutoriaAuxiliar.codigo='';
  }

  consultaIndividual(codigo:string){
    this.cliente.getTutorias(codigo).then(respuesta=>{
      this.tutoriaAuxiliar= <Tutoria>respuesta ;
      //console.log(respuesta)
    })             
    .catch(error=>{
      console.log(error)
    })             
  }  
  
 

  agregar(){
    this.cliente.postTutorias(this.tutoriaAuxiliar).then(respuesta=>{

      this.mostrarMensaje('Se agregó la Tutoría correctamente',2000);
      
    }).catch(error=>{
      this.mostrarMensaje('No se pudo agregar la Tutoría', 2000)
    })
  }
  agregarAlumno()
  {
    this.tutoriaAuxiliar.alumnos.push(this.alumnoAuxiliar);
  }
  agregarDetalle(){
   // this.tutoriaAuxiliar.detalles.push(this.detalleAuxiliar)
  }
                
  Consultar(identificacion: string) {
    this.cliente.getTutorias().then(respuesta => {
      this.tutoriasAuxiliar = [];
 

      for (let elemento in respuesta) {
        console.log("elemento=" + elemento)
        this.cliente.getTutorias(elemento).then(respuesta_clase => {
          this.tutoriaAuxiliar = <Tutoria>respuesta_clase;
          console.log("nombre_clase=" + this.tutoriaAuxiliar.identDocente)
         
      
          if (identificacion === this.tutoriaAuxiliar.identDocente) { 
            this.tutoriasAuxiliar.push(respuesta[elemento]);
        
           
          
          }
          console.log(this.Detalle.length);
          
        })
          .catch(error => {
            console.log(error)
          })


      }
     

    })
  }

 
}


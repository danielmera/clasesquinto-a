export interface ICrud{
    nueva():void;
    agregar():void;
    consultar():void;
    eliminar():void;
}
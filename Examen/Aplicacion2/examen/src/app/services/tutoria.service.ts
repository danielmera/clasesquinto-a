import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Tutoria } from '../interfaces/tutoria';

@Injectable({
  providedIn: 'root'
})
export class TutoriaService {
  apiURL=`https://proyecto2-43a48.firebaseio.com/Tutorias`

  constructor(private clienteServicio:HttpClient) { }

                 
  //Consultar las Tutorías
  public getTutorias(codigo=''){
    if(codigo=='')
    return this.clienteServicio.get(`${this.apiURL}.json`).toPromise()
    return this.clienteServicio.get(`${this.apiURL}/${codigo}.json`).toPromise()
    
  }               
  //Insertar Tutorías
  public postTutorias(tutoriax: Tutoria){
    return this.clienteServicio.put(`${this.apiURL}/${tutoriax.codigo}.json`, 
    tutoriax, {headers:{'Content-Type':'application/json'}}).toPromise();
  }


  //EliminarCurso
  public deleteTutorias(tutoriax:string){
    return this.clienteServicio.delete(`${this.apiURL}/${tutoriax}.json`).toPromise();
  }
}

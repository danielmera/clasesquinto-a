var db = firebase.database();

var update = document.getElementById('update');
update.disabled = true;

function value(request){
    return  document.getElementById(request).value;
}
function asignation(request,response){
    return  document.getElementById(request).value=response;
}
function printHTML(request,response){
    return document.getElementById(request).innerHTML+=response;
}                   
function inHTML(request,response){
    return document.getElementById(request).innerHTML=response;
}
function dateActuality(){
    var fh = new Date();
    return fh.getFullYear()+"-"+(fh.getMonth()+1)+"-"+fh.getDate()+" "+fh.getHours()+":"+fh.getMinutes();
}
function insertTask(codigo,detalle, asignatura, docente, temas, silabo, motivo){
    db.ref('Tutorias/').push({
        codigo:codigo,
        detalle:detalle,             
        date:dateActuality(),                
        asignatura:asignatura,
        docente:docente,
        temas:temas,
        silabo:silabo,
        motivo:motivo
        
    });
}
function onClickInsert(){           
    var codigo = value("codigo");
    var detalle = value("detalle");
    var asignatura= value("asignatura");
    var docente= value("docente");
    var tema= value("tema");
    var silabo= value("silabo");
    var motivo= value("motivo");
   // var userSelect= value("userSelect");
    if(codigo.length==0 || detalle.length==0 ||asignatura.length==0 || docente.length==0 || temas.length==0 ||silabo.length==0 || motivo.length==0){ 
        alert("Campo Vacío"); 
    }else{ 
        inHTML("loadTable","");
        insertTask(codigo,detalle, asignatura, docente, tema, silabo, motivo); 
        asignation("codigo","");
        asignation("detalle","");
        asignation("asignatura","");
        asignation("docente","");
        asignation("temas", " ");
        asignation("silabo", " ");
        asignation("motivo", "");
        
        alert("Se ha guardado satisfactoriamente");
    }
}
function updateTask(codigo,detalle,key, asignatura, docente, temas, silabo, motivo){
    db.ref('Tutorias/'+key).update({
        codigo:codigo,
        detalle:detalle,
        date:dateActuality(),
        asignatura:asignatura,
        docente:docente,
        temas:temas,
        silabo:silabo,
        motivo:motivo
       
    });
}
function onClickUpdate(){
    var codigo = value("codeEdit");
    var detalle = value("userSelect");
    var key = value("key"); 
    var asignatura= value("asigEdit");
    var docente= value("docenEdit");
    var temas= value("temEdit");
    var silabo= value("silaEdit");
    var motivo= value("motivoEdit");
   // var userSelect= value("userSelectEdit");
    if(codigo.length==0 || detalle.length==0 ||asignatura.length==0 || docente.length==0 || temas.length==0 ||silabo.length==0 ||motivo.length==0) { 
        alert("empty field"); 
    }else{ 
        inHTML("loadTable","");
        updateTask(codigo,detalle,key, asignatura, docente, temas, silabo, motivo); 
        inHTML("editData","");
        alert("Modificada satisfactoriamente");
        update.disabled = true;
    }
}
/* function removeTask(key){
    if(confirm("¿Quieres borrar la tutoría?")){
        inHTML("loadTable","");
        db.ref('tutorias/'+key).remove();
    }
} */
function table(codigo,detalle,date,key, asignatura, docente, temas, silabo, motivo){
    return '<tr><td>'+codigo+'</td><td>'+detalle+'</td><td>'+date+'</td><td>'
    +asignatura+'</td><td>'+docente+'</td><td>'+temas+'</td><td>'+silabo+'</td><td>'+motivo+'</td>'+
    '<td><a href="#" onclick="viewDataUpdate(\''+codigo+'\',\''+detalle+'\',\''+key+'\',\''+asignatura+'\',\''+docente+'\',\''+temas+'\',\''+silabo+'\',\''+motivo+'\')">'+
    '<i class="fas fa-edit blue icon-lg"></i></a></td>'
    ;
}
function viewDataUpdate(codigo,detalle,key, asignatura, docente, temas, silabo, motivo){
    var response = '<div class="form-group"><input type="hidden" value='+key+' id="key">' +
    '<input type="text" id="codeEdit" class="form-control" placeholder="Name" value='+codigo+'>'+
    '</div>'+
    '<div class="form-group">'+
    '<select id="userSelect" class="browser-default">' +
        '<option value=Ninguna>'+ "Ninguna" + '</option>'+
        '<option value=Buena>'+ "Buena" + '</option>'+
        '<option value=Mala>'+ "Mala" + '</option>'+
        '<option value=Media>'+ "Media" + '</option>'+
    '</select>'+
    '<textarea placeholder="DescriptionEdit" class="form-control" id="userSelect">'+detalle+'</textarea>'+
    '</div>'+
    '<div class="form-group">'+
    '<input type="text" id="asigEdit" class="form-control" placeholder="AsignaturaEdit" value='+asignatura+'>'+
    '</div>'+
    '<div class="form-group">'+
    '<input type="text" id="docenEdit" class="form-control" placeholder="DocentEdit" value='+docente+'>'+
    '</div>'+
    '<div class="form-group">'+
    '<input type="text" id="temEdit" class="form-control" placeholder="TemtEdit" value='+temas+'>'+
    '</div>'+
    '<div class="form-group">'+
    '<input type="text" id="silaEdit" class="form-control" placeholder="SilatEdit" value='+silabo+'>'+
    '</div>'+
    '<div class="form-group">'+
    '<textarea placeholder="MotivoEdit" class="form-control" id="motivoEdit">'+motivo+'</textarea>'+
    '</div>';
    inHTML('editData',response);
    update.disabled = false;
}
var reference = db.ref('Tutorias/'+name);    
reference.on('value',function(datas){
    var data = datas.val();
    $.each(data, function(nodo, value) {
            var sendData = table(value.codigo,value.detalle,value.date,nodo, value.asignatura, value.docente, value.temas, value.silabo, value.motivo);
            printHTML('loadTable',sendData);
    });       
});

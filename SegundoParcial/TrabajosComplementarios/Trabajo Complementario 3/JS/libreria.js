window.addEventListener('load', function(){
    btnnuevo.addEventListener('click', function(){
        txtCedula.value="";
        txtNombre.value="";
        txtCorreo.value="";
        txtTelefono.value="";
        txtContrasenha.value="";                  
    })
    //URL Firebase
    btnconsultar.addEventListener('click', function(){
        let url= `https://segundoparcialproyecto-15b53.firebaseio.com/Registro.json`
        fetch(url).then(resultado=>{
            return resultado.json();
        }).then(resultado2=>{
            let tablaHTML="<table border=1>";
            for(let elemento in resultado2){
                tablaHTML+= "<tr>"

                    tablaHTML+= `<td>${resultado2[elemento].cedula}</td>   
                                <td>${resultado2[elemento].nombre}</td>
                                <td>${resultado2[elemento].telefono}</td>
                                <td>${resultado2[elemento].correo}</td>
                                <td>${resultado2[elemento].contrasenha}</td>
                                `
                    tablaHTML+= "</tr>"
            }
            tablaHTML+="</table>"

            divconsulta.innerHTML= tablaHTML;


             document.querySelectorAll.forEach(elemento=>{
                elemento.addEventListener('click', function(){
                    let url2= `https://segundoparcialproyecto-15b53.firebaseio.com/Registro/${elemento.innerHTML.trim()}.json`
                    console.log(url2)

                    fetch(url2).then(resultado=>{return resultado.json()}).then(resultado2=>{
                        txtCedula.value=resultado2.cedula;
                        txtNombre.value=resultado2.nombre;
                        txtTelefono.value=resultado2.telefono;
                        txtCorreo.value=resultado2.correo;
                        txtContrasenha.value=resultado2.contraseha;
                    })
                })
            }) 
            
        }).catch(error=>{
            console.log(error)
        })
    })

    btngrabar.addEventListener('click',function(){
        //MÉTODO POST
        let url= `https://segundoparcialproyecto-15b53.firebaseio.com/Registro.json`
        let contenido={cedula:txtCedula.value, nombre:txtNombre.value, telefono:txtTelefono.value, correo:txtCorreo.value,
                    contrasenha:txtContrasenha.value}
                  
        fetch(url,{
            method:'POST',
            body: JSON.stringify(contenido),
            headers:{
                'Content-Type':'application/json'
            }
        }).then(resultado=>{
            return resultado.json();
        }).then(resultado2=>{
            console.log(resultado2.name)
        }).catch(error=>{
            console.error('Hubo un error al guardarse la información', error)
        })
    })

    btngrabar2.addEventListener('click',function(){
        //MÉTODO POST
        let url= `https://segundoparcialproyecto-15b53.firebaseio.com/Registro/${txtCedula.value}.json`
        let contenido={cedula:txtCedula.value, nombre:txtNombre.value, telefono:txtTelefono.value, correo:txtCorreo.value,
                    contrasenha:txtContrasenha.value}
                  
        fetch(url,{
            method:'PUT',
            body: JSON.stringify(contenido),
            headers:{
                'Content-Type':'application/json'
            }
        }).then(resultado=>{
            return resultado.json();
        }).then(resultado2=>{
            console.log(resultado2.name)
        }).catch(error=>{
            console.error('Hubo un error al guardarse la información', error)
        })
    })              

    btneliminar.addEventListener('click', function(){
        let url= `https://segundoparcialproyecto-15b53.firebaseio.com/Registro/${txtCedula.value}.json`

        fetch(url, {
            method:'DELETE'
        })
        .then(resultado=>{
            return resultado.json()
        })
        .then(resultado2=>{
            console.log(resultado2)
        })
        .catch(error=>{
            console.error('No se puede eliminar el usuario', error)
        })
    })
    
})
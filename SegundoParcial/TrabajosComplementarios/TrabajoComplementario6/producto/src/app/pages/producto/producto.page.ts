import { Component, OnInit } from '@angular/core';
import { Producto } from 'src/app/interfaces/producto';
import { ICrudProducto } from 'src/app/interfaces/ICrudProducto';  
import {ProductoService} from 'src/app/services/producto.service';
import { ToastController } from '@ionic/angular';
       
                        
@Component({
  selector: 'app-producto',           
  templateUrl: './producto.page.html',
  styleUrls: ['./producto.page.scss'],         
})
export class ProductoPage implements OnInit, ICrudProducto {

  public productoAuxiliar: Producto = {codigo:'000',nombre:'Vacío', precio:0,cantidad:0} ;
  

  public productosAuxiliar: Producto[] = [] ;                           
                  
  public total:number=0;           
  constructor(private cliente: ProductoService, private toast: ToastController) { }  
  
  async mostrarMensaje(mensaje:string, duracion:number){
    // convertimos en sIncrono o esperamos que la funciOn cree el toast 
     const mensajex= await this.toast.create({message:mensaje, duration:duracion });
     // ya estamos seguros que el toast esta creado, y podemos invocar el mEtodo present
     mensajex.present();
 }
 grabar(): void {
  this.cliente.postProducto(this.productoAuxiliar).then(respuesta=>{

    this.mostrarMensaje('se grabó correctamente',2000);
    //console.log('grabO correctamente')
            

  }).catch(error=>{
    console.log('no se pudo almacenar el producto')
  })
  
}consultar(): void {

  this.cliente.getProducto().then(respuesta=>{
    this.productosAuxiliar=[];
    for (let elemento in respuesta)
    {
      this.productosAuxiliar.push(respuesta[elemento]);
    }

  })
  .catch(err=>{
    console.log(err)
  })
}
consultaIndividual(codigox:string){
  this.cliente.getProducto(codigox).then(respuesta=>{
    this.productoAuxiliar= <Producto>respuesta ;
  })
  .catch(error=>{
    console.log(error)
  })
}
eliminar(): void {
  this.cliente.deleteProducto(this.productoAuxiliar.codigo).then(respuesta=>{
    console.log('se eliminO correctamente')
  })
  .catch(error=>{
    console.log('No se pudo eliminar el curso')
  })
}

ngOnInit() {

}
nuevo()
{
  
  this.productoAuxiliar={ nombre:'',precio:0, cantidad:0,codigo:'' };
}


}
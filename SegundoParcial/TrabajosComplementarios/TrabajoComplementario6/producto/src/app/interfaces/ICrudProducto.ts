export interface ICrudProducto
{
    nuevo():void;
    grabar():void;
    consultar():void;
    eliminar():void;
}
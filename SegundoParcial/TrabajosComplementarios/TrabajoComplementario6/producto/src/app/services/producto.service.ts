import { Injectable } from '@angular/core';
import { Producto } from '../interfaces/producto';
import{HttpClient} from  '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProductoService {        

  apiURL=`https://proyectosemana13-c8b3f.firebaseio.com/Productos`

  constructor(private clienteServicio: HttpClient) { }

  public getProducto(codigo='')            
  {
    if(codigo==' ')
    return this.clienteServicio.get(`${this.apiURL}.json`).toPromise()
    return this.clienteServicio.get(`${this.apiURL}/${codigo}.json`).toPromise()

  }

  public postProducto(productox: Producto)
  {
    

    return this.clienteServicio.put(`${this.apiURL}/${productox.codigo}.json`
    , productox ,{ headers:{'Content-Type': 'application/json'}}).toPromise();
  }

  //eliminar un curso según su código
  public deleteProducto(codigox:string)
  {
     return this.clienteServicio.delete(`${this.apiURL}/${codigox}.json`).toPromise();
  }
}

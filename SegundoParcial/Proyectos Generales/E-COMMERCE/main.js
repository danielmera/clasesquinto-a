//window.addEventListener('load', function(){
   
   //GLOBAL
   var products=[];
   var cartItems=[];
   var cart_n= document.getElementById('cart_n');
   //DIVS
   var fruitDIV= document.getElementById("fruitDIV");
   var juiceDIV= document.getElementById("juiceDIV");
   var saladDIV= document.getElementById("saladDIV");

    //INFORMACIÓN                            

    var JUICE=[
        {name:'220V', price:1},
        {name:'Agua', price:1},                     
        {name:'AguaDeCoco', price:2},
        {name:'Chocolate', price:1},
        {name:'Heineken', price:2},    
  
  
  ];                               
    var FRUIT=[
        {name:'Aguacate', price:1},
        {name:'Limon', price:2},
        {name:'Manzana', price:1},
        {name:'Papaya', price:2},
  
  
  ];
    //HTML 
    function HTMLfruitProduct(con){
        let URL = `img/frutas/fruit${con}.jpg`;
        let btn = `btnFruit${con}`;

        return `
        <div class ="col s3 wow fadeInUp data-wow-delay= "3s" data-wow-offset= "300" ">
        <div class= "card">
        <div class="card-image">
        <img src = "${URL}">
        <a id="${btn}" onclick="cart('${FRUIT[con-1].name} ',' ${FRUIT[con-1].price}
        ' , ' ${URL}' , ' ${con}' , '${btn}')" class="btn btn-floating
        halfway-fab waves-effect waves-light red">
        <i class="material-icons"> add </i>
        </a>
        </div>
        <div class="card-content">
        <i class="material-icons" style= "color:orange">star</i>
        <i class="material-icons" style= "color:orange">star</i>
        <i class="material-icons" style= "color:orange">star</i>
        <i class="material-icons" style= "color:orange">star</i>
        <i class="material-icons" style= "color:orange">star</i>
            <span class= "card-tittle"> ${FRUIT[con-1].name}</span>
            <p> Price: ${FRUIT[con-1].price}</p>
        </div>
       </div>
    </div>
`
}

function HTMLbebidaProduct(con){
    let URL = `img/bebidas/bebida${con}.jpg`;
    let btn = `btnBebida${con}`;

    return `
    <div class ="col s3 wow fadeInUp data-wow-delay= "3s" data-wow-offset= "300" ">
    <div class= "card">
    <div class="card-image">
    <img src = "${URL}">
    <a id="${btn}" onclick="cart('${JUICE[con-1].name} ',' ${JUICE[con-1].price}
    ' , ' ${URL}' , ' ${con}' , '${btn}')" class="btn btn-floating
    halfway-fab waves-effect waves-light red">
    <i class="material-icons"> add </i>
    </a>
    </div>
    <div class="card-content">
        <i class="material-icons" style= "color:orange">star</i>
        <i class="material-icons" style= "color:orange">star</i>
        <i class="material-icons" style= "color:orange">star</i>
        <i class="material-icons" style= "color:orange">star</i>
        <i class="material-icons" style= "color:orange">star</i>
        <span class= "card-tittle"> ${JUICE[con-1].name}</span>
        <p> Price: ${JUICE[con-1].price}</p>
    </div>
   </div>
</div>
`
}


   //ANIMATION
   function animation(){
       const Toast = Swal.mixin({
           toast:true,
           position:'top-end',
           showConfirmButton: false,
           timer:2000
       });
       Toast.fire({
           type: 'success',
           title: 'Added to shopping cart'
       })
   }
   //CART FUNCTIONS
   function cart(name, price, url, con, btncart){
       var item={
           name:name,
           price:price,
           url:url
       }
       carItems.push(item);
       let storage = JSON.parse(localStorage.getItem("cart"));
       if(storage==null){
            products.push(item);
            localStorage.setItem("cart", JSON.stringify(products));
       }else{
           products= JSON.parse(localStorage.getItem("cart"));
           products.push(item);
           localStorage.setItem("cart",JSON.stringify(products));
       }
       products = JSON.parse(localStorage.getItem('cart'));
       cart_n.innerHTML= `[${products.length}]`;
       document.getElementById(btncart).style.display='none';
       animation();
   }
    //RENDER

    $('.carousel.carousel-slider').carousel({
        fullWidth:true,
        indicators:true
    });
    $(document).ready(function(){
        $('.modal').modal();
    });
    function render(){
        new WOW().init();
        for(let index = 1; index <=4; index++){
            fruitDIV.innerHTML+= `${HTMLfruitProduct(index)}`;  
        }
         for(let index = 1; index <=5; index++){
            juiceDIV.innerHTML+=`${HTMLbebidaProduct(index)}`;
           // dulcesDIV.innerHTML+=`${HTMLdulcesProduct}`;
        }
       
        if(localStorage.getItem('cart')==null){

        }else{
            products=JSON.parse(localStorage.getItem('cart'));
            cart_n.innerHTML=`[${products.length}]`;
        }              
    }
//})
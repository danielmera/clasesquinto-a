import { Injectable } from '@angular/core';
import { Market } from '../interfaces/market';
import{HttpClient} from  '@angular/common/http';
@Injectable({             
  providedIn: 'root'                       
})
export class MarketService {
  apiURL=`https://mercadoonline-467e8.firebaseio.com/order`
  constructor(private clienteServicio: HttpClient) { }
  public getProducto(id='')            
  {
    if(id==' ')
    return this.clienteServicio.get(`${this.apiURL}.json`).toPromise()
    return this.clienteServicio.get(`${this.apiURL}/${id}.json`).toPromise()

  }
  public postProducto(productox: Market)
  {
    return this.clienteServicio.put(`${this.apiURL}/${productox.id}.json`
    , productox ,{ headers:{'Content-Type': 'application/json'}}).toPromise();
  }
  public deleteProducto(codigox:string)
  {
     return this.clienteServicio.delete(`${this.apiURL}/${codigox}.json`).toPromise();
  }
}
                    
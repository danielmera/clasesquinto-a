import { Component, OnInit } from '@angular/core';
import { ICrud } from 'src/app/interfaces/ICrud';
import { Market } from 'src/app/interfaces/market';
import { ToastController } from '@ionic/angular';
import {MarketService} from 'src/app/services/market.service';
                 

@Component({
  selector: 'app-market',                                          
  templateUrl: './market.page.html',            
  styleUrls: ['./market.page.scss'],                                  
})
export class MarketPage implements OnInit, ICrud {
  public productoAuxiliar: Market = {id:'',order:0, products:'', date:'', hour:'',  payment:'', userEmail:'', userName:'', total:0, estado:''} ;
  public ProductsAuxiliar: {codigo:number, name:string, price:number, amount:number}[]=
  [
    {codigo:0, name:'Aguacate', price:1, amount:1},                
    {codigo:1, name:'Limón', price:2, amount:1},                    
    {codigo:2, name:'Manzana', price:2, amount:1},
    {codigo:3, name:'220V', price:1, amount:1} ,  
    {codigo:4, name:'Pizza Salami', price:8.99, amount:1},                
    {codigo:5, name:'Pizza Classic', price:5.49, amount:1},
    {codigo:6, name:'Sliced Bread', price:4.99, amount:1},
    {codigo:7, name:'Salad', price:6.99, amount:1},
    {codigo:8, name:'Agua de Coco', price:2, amount:1}  
  
          
  ];
  public productsAuxiliar:{hour: string}
  
  public productosAuxiliar: Market[] = [] ;                           
                           
  public total:number=0;   
  public myDate: String = new Date().toISOString();
  
  public fecha: String = new Date().toISOString();
  constructor(private cliente: MarketService, private toast: ToastController) { }
         
  async mostrarMensaje(mensaje:string, duracion:number){
    // convertimos en sIncrono o esperamos que la funciOn cree el toast 
     const mensajex= await this.toast.create({message:mensaje, duration:duracion });
     // ya estamos seguros que el toast esta creado, y podemos invocar el mEtodo present
     mensajex.present();
 }
 grabar(): void {
  this.cliente.postProducto(this.productoAuxiliar).then(respuesta=>{

    this.mostrarMensaje('se grabó correctamente',2000);
    //console.log('grabO correctamente')
            
               
  }).catch(error=>{
    console.log('no se pudo almacenar el producto')
  })
  
}consultar(): void {

  this.cliente.getProducto().then(respuesta=>{
    this.productosAuxiliar=[];
    for (let elemento in respuesta)
    {
      this.productosAuxiliar.push(respuesta[elemento]);
    }

  })
  .catch(err=>{    
    console.log(err)
  })
}
consultaIndividual(codigox:string){
  this.cliente.getProducto(codigox).then(respuesta=>{
    this.productoAuxiliar= <Market>respuesta ;
  })             
  .catch(error=>{
    console.log(error)
  })             
}          
eliminar(): void {
  this.cliente.deleteProducto(this.productoAuxiliar.id).then(respuesta=>{
    console.log('se eliminó correctamente')
  })
  .catch(error=>{
    console.log('No se pudo eliminar el producto')
  })
}

ngOnInit() {
                      
}
nuevo()
{
  
  this.productoAuxiliar={ order:0,products:'', payment:'',date:'', hour:'', userEmail:'', userName:'',total:0, id:'', estado:'' };
}




}


